The first step to migrating a project from SVN to Git-based version control is to prepare the migration lead’s local machine. In this phase, you’ll download a convenient utility script, mount a case-sensitive filesystem (if necessary), and map author information from SVN to Git.

All of the the following steps should be performed on the migration lead’s local machine.

### Download the migration script

Git comes with most of the necessary tools for importing an SVN repository; however, there are a few missing bits of functionality that Atlassian has rolled into a handy JAR file. This file will be integral to the migration, so be sure to download [`svn-migration-scripts.jar`](https://bitbucket.org/atlassian/svn-migration-scripts/downloads) from Atlassian’s Bitbucket account. This guide assumes that you’ve saved it in your home directory.

Once you’ve downloaded it, it’s a good idea to verify the scripts to make sure you have the [Java Runtime Environment](http://www.java.com/en/download/help/download_options.xml), [Git](http://git-scm.com/downloads), [Subversion](http://subversion.apache.org/packages.html), and the [git-svn](https://www.kernel.org/pub/software/scm/git/docs/git-svn.html) utility installed. Open a command prompt and run the following:

```
java -jar ~/svn-migration-scripts.jar verify
```

This will display an error message in the console if you don’t have the necessary programs for the migration process. Make sure that any missing software is installed before moving on.

If you get a warning about being unable to determine a version, run `export LANG=C` (*nix) or `SET LANG=C` (Windows) and try again.

If you’re performing the migration on a computer running OS X, you’ll also see the following warning:

```
You appear to be running on a case-insensitive file-system. This is unsupported, and can result in data loss.
```

We’ll address this in the next section.

### Mount a case-sensitive disk image

Migrating to Git should be done on a case-sensitive file system to avoid corrupting the repository. This is a problem if you’re performing the migration on an OS X computer, as the OS X filesystem isn’t case-sensitive.

If you’re not running OS X, all you need to do is create a directory on your local machine called `~/GitMigration`. This is where you will perform the conversion. After that, you can skip to the next section.

![Git migration: mount disk image](/images/tutorials/migrating/migrating-prepare/01.svg)

If you are running OS X, you need to mount a case-sensitive disk image with the `create-disk-image` script included in `svn-migration-scripts.jar`. It takes two parameters:

1.  The size of the disk image to create in gigabytes. You can use any size you like, as long as it’s bigger than the SVN repository that you’re trying to migrate.
2.  The name of the disk image. This guide uses `GitMigration` for this value. 

For example, the following command creates a 5GB disk image called `GitMigration`:

```
java -jar ~/svn-migration-scripts.jar create-disk-image 5 GitMigration
```

The disk image is mounted in your home directory, so you should now see a directory called `~/GitMigration` on your local machine. This serves as a virtual case-sensitive filesystem, and it’s where you’ll store the converted Git repository.

### Extract the author information

SVN only records the username of the author for each revision. Git, however, stores the full name and email address of the author. This means that you need to create a text file that maps SVN usernames to their Git counterparts.

![Git migration: Map SVN usernames to Git usernames](/images/tutorials/migrating/migrating-prepare/02.svg)

Run the following commands to automatically generate this text file:

```
cd ~/GitMigration 
java -jar ~/svn-migration-scripts.jar authors <svn-repo> > authors.txt
```

Be sure to replace `<svn-repo>` with the URI of the SVN repository that you want to migrate. For example, if your repository resided at `https://svn.example.com`, you would run the following:

```
java -jar ~/svn-migration-scripts.jar authors https://svn.example.com > authors.txt
```

This creates a text file called `authors.txt` that contains the username of every author in the SVN repository along with a generated name and email address. It should look something like this:

```
j.doe = j.doe <j.doe@mycompany.com> 
 m.smith = m.smith <m.smith@mycompany.com>
```

Change the portion to the right of the equal sign to the full name and email address of the corresponding user. For example, you might change the above authors to:

```
j.doe = John Doe <john.doe@atlassian.com>
 m.smith = Mary Smith <mary.smith@atlassian.com>
```

### Summary

Now that you have your migration scripts, disk image (OS X only), and author information, you’re ready to import your SVN history into a new Git repository. The next phase explains how this conversion works.
