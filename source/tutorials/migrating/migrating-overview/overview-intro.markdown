We’ve broken down the SVN-to-Git migration process into 5 simple steps:

1. Prepare your environment for the migration.
2. Convert the SVN repository to a local Git repository.
3. Synchronize the local Git repository when the SVN repository changes.
4. Share the Git repository with your developers via Bitbucket.
5. Migrate your development efforts from SVN to Git. 

The prepare, convert, and synchronize steps take a SVN commit history and turn it into a Git repository. The best way to manage these first 3 steps is to designate one of your team members as the migration lead (if you’re reading this guide, that person is probably you). All 3 of these steps should be performed on the migration lead’s local computer.

![Git Migration: Prepare, clone, sync](/images/tutorials/migrating/migrating-overview/01.svg)

After the synchronize phase, the migration lead should have no trouble keeping a local Git repository up-to-date with an SVN counterpart. To share the Git repository, the migration lead can share his local Git repository with other developers by pushing it to [Bitbucket](http://bitbucket.org), a Git hosting service.

![Git Migration: Share the git repo via bitbucket](/images/tutorials/migrating/migrating-overview/02.svg)

Once it’s on Bitbucket, other developers can clone the converted Git repository to their local machines, explore its history with Git commands, and begin integrating it into their build processes. However, we advocate a one-way synchronization from SVN to Git until your team is ready to switch to a pure Git workflow. This means that everybody should treat their Git repository as read-only and continue committing to the original SVN repository. The only changes to the Git repository should happen when the migration lead synchronizes it and pushes the updates to Bitbucket.

This provides a clear-cut transition period where your team can get comfortable with Git without interrupting your existing SVN-based workflow. Once you’re confident that your developers are ready to make the switch, the final step in the migration process is to freeze your SVN repository and begin committing with Git instead.

![Git migration: Migrate Active Development to Git](/images/tutorials/migrating/migrating-overview/03.svg)

This switch should be a very natural process, as the entire Git workflow is already in place and your developers have had all the time they need to get comfortable with it. By this point, you have successfully migrated your project from SVN to Git.
