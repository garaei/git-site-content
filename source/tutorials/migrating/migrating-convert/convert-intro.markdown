The next step in the migration from SVN to Git is to import the contents of the SVN repository into a new Git repository. We’ll do this with the `git svn` utility that is included with most Git distributions, then we’ll clean up the results with `svn-migration-scripts.jar`.

Beware that the conversion process can take a significant amount of time for larger repositories, even when cloning from a local SVN repository. As a benchmark, converting a 400MB repository with 33,000 commits on master took around 12 hours to complete.

For reasonably sized repositories, the following steps should be run on the migration lead’s local computer. However, if you have a very large SVN repository and want to cut down on the conversion time, you can run `git svn clone` on the SVN server instead of on the migration lead’s local machine. This will avoid the overhead of cloning via a network connection.

### Clone the SVN repository

The `git svn clone` command transforms the trunk, branches, and tags in your SVN repository into a new Git repository. Depending on the structure of your SVN repo, the command needs to be configured differently.

![Git migration: git svn clone command](/images/tutorials/migrating/migrating-convert/01.svg)

**Standard SVN layouts**

If your SVN project uses the standard `/trunk`, `/branches`, and `/tags` directory layout, you can use the `--stdlayout` option instead of manually specifying the repository’s structure. Run the following command in the `~/GitMigration` directory:

```
git svn clone --stdlayout --authors-file=authors.txt
 <svn-repo>/<project> <git-repo-name>
```

Where `<svn-repo>` is the URI of the SVN repository that you want to migrate and, `<project>` is the name of the project that you want to import, and `<git-repo-name>` is the directory name of the new Git repository.

For example, if you were migrating a project called `Confluence`, hosted on `https://svn.atlassian.com`, you might run the following:

```
git svn clone --stdlayout --authors-file=authors.txt
 https://svn.atlassian.com/Confluence ConfluenceAsGit
```

**Non-standard SVN layouts**

If your SVN repository doesn’t have a standard layout, you need to provide the locations of your trunk, branches, and tags using the `--trunk`, `--branches`, and `--tags` command line options. For example, if you have branches stored in both the `/branches` directory and the `/bugfixes` directories, you would use the following command:

```
git svn clone --trunk=/trunk --branches=/branches 
 --branches=/bugfixes --tags=/tags --authors-file=authors.txt 
 <svn-repo>/<project> <git-repo-name>
```

### Inspect the new Git repository

After `git svn clone` has finished (this might take a while), you’ll find a new directory called `<git-repo-name>` in `~/GitMigration`. This is the converted Git repository. You should be able to switch into `<git-repo-name>` and run any of the standard Git commands to explore your project.

Branches and tags are not imported into the new Git repository as you might expect. You won’t find any of your SVN branches in the `git branch` output, nor will you find any of your SVN tags in the `git tag` output. But, if you run `git branch -r`, you’ll find all of the branches and tags from your SVN repository. The `git svn clone` command imports your SVN branches as remote branches and imports your SVN tags as remote branches prefixed with `tags/`.

![Git migration: structure of cloned Git repo](/images/tutorials/migrating/migrating-convert/02.svg)

This behavior makes certain two-way synchronization procedures easier, but it can be very confusing when trying to make a one-way migration Git. That’s why our next step will be to convert these remote branches to local branches and actual Git tags.

### Clean the new Git repository

The `clean-git` script included in `svn-migration-scripts.jar` turns the SVN branches into local Git branches and the SVN tags into full-fledged Git tags. Note that this is a **destructive** operation, and you will not be able to move commits from the Git repository back into the SVN repository.

If you’re following this migration guide, this isn’t a problem, as it advocates a one-way sync from SVN to Git (the Git repository is considered read-only until after the [Migrate](/tutorials/migrating-migrate) step). However, if you’re planning on committing to the Git repository _and_ the SVN repository during the migration process, you should not perform the following commands. This is an advanced task, as is not recommended for the typical project.

To see what can be cleaned up, run the following command in `~/GitMigration/<git-repo-name>`:

```
java -Dfile.encoding=utf-8 -jar ~/svn-migration-scripts.jar clean-git
```

This will output all of the changes the script wants to make, but it won’t actually make any of them. To execute these changes, you need to use the `--force` option, like so:

```
java -Dfile.encoding=utf-8 -jar ~/svn-migration-scripts.jar clean-git 
--force
```

You should now see all of your SVN branches in the `git branch` output, along with your SVN tags in the `git tag` output. This means that you’ve successfully converted your SVN project to a Git repository.

### Summary

In this step, you turned an SVN repository into a new Git repository with the `git svn clone` command, then cleaned up the structure of the resulting repository with `svn-migration-scripts.jar`. In the next step, you’ll learn how to keep this new Git repo in sync with any new commits to the SVN repository. This will be a similar process to the conversion, but there are some important workflow considerations during this transition period.
