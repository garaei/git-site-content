This migration guide advocates a one-way synchronization from SVN to Git during the transition period. This means that while your team is getting comfortable with Git, they should still only be committing to the original SVN repository. When you’re ready to make the switch, the SVN repository should freeze at whatever state it’s in. Then, developers should begin committing to their local Git repositories and sharing them via Bitbucket.

![Git migration: One-way synchronization from SVN to Git](/images/tutorials/migrating/migrating-migrate/01.svg)

The discrete switch from SVN to Git makes for a very intuitive migration. All of your developers should already understand the new Git workflows that they’ll be using, and they should have had plenty of time to practice using Git commands on the local repositories they cloned from Bitbucket.

This page guides you through the final step of the migration.

### Synchronize the Git repository

Before finalizing your migration to Git, you should make sure that your Git repository contains any new changes that have been committed to your SVN repository. You can do this with the same process described in the [Synchronize](/tutorials/migrating-synchronize) phase.

```
git svn fetch
java -Dfile.encoding=utf-8 -jar ~/svn-migration-scripts.jar sync-rebase
java -Dfile.encoding=utf-8 -jar ~/svn-migration-scripts.jar clean-git --force
```

### Backup the SVN repository

While you can still see your pre-Git project history in the migrated repository, it’s a good idea to backup the SVN repository just in case you ever need to explore the raw SVN data. An easy way to backup an SVN repo is to run the following on the machine that hosts the central SVN repository. If your SVN repo is hosted on a Linux machine, you can use the following:

```
svnadmin dump <svn-repo> | gzip -9 > <backup-file>
```

Replace `<svn-repo>` with the file path of the SVN repository that you’re backing up, and replace `<backup-file>` with the file path of the compressed file containing the backup.

### Make the SVN repository read-only

All of your developers should now be committing with Git. To enforce this convention, you can make your SVN repository read-only. This process can vary depending on your server setup, but if you’re using the `svnserve` daemon, you can accomplish this by editing your SVN repo’s `conf/svnserve.conf` file. It’s `[general]` section should contain the following lines:

```
anon-access = read
auth-access = read
```

This tells `svnserve` that both anonymous and authenticated users only have read permissions.

### Summary

And that’s all there is to migrating a project to Git. Your team should now be developing with a pure Git workflow and enjoying all of the benefits of distributed development. Good job!
