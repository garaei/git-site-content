It’s very easy to synchronize your Git repository with new commits in the original SVN repository. This makes for a comfortable transition period in the migration process where you can continue to use your existing SVN workflow, but begin to experiment with Git.

It’s possible to synchronize in both directions. However, we recommend a one-way sync from SVN to Git. During your transition period, you should only commit to your SVN repository, not your Git repo. Once you’re confident that your team is ready to make the switch, you can complete the migration process and begin to commit changes with Git instead of SVN.

![Git migration: we recommend a one-way sync from SVN to Git.](/images/tutorials/migrating/migrating-synchronize/01.svg)

In the meantime, you should continue to commit to your SVN repository and synchronize your Git repository whenever necessary. This process is similar to the [Convert](/tutorials/migrating-convert) phase, but since you’re only dealing with incremental changes, it should be much more efficient.

### Update the authors file

The `authors.txt` file that we used to map SVN usernames to full names and email addresses is essential to the synchronization process. If it has been moved from the `~/GitMigration/authors.txt` location that we’ve been using thus far, you need to update its location with:

```
git config svn.authorsfile <path-to-authors-file>
```

If new developers have committed to the SVN repository since the last sync (or the initial clone), the authors file needs to be updated accordingly. You can do this by manually appending new users to `authors.txt`, or you can use the `--authors-prog` option, as discussed in the next section.

For one-off synchronizations it’s often easier to directly edit the authors file; however, the-`--authors-prog` option is preferred if you’re performing unsupervised syncs (i.e. in a scheduled task).

### Automatically generating Git authors

If your authors file doesn’t need to be updated, you can skip to the next section.

The `git svn` command includes an option called `--authors-prog`, which points to a script that automatically transforms SVN usernames into Git authors. You’ll need to configure this script to accept the SVN username as its only argument and return a single line in the form of `Name <email>` (just like the right hand side of the existing authors file). This option can be very useful if you need to periodically add new developers to your project.

If you want to use the `--authors-prog` option, create a file called `authors.sh` option in `~/GitMigration`. Add the following line to `authors.sh` to return a dummy Git name and email for any authors that aren’t found in `authors.txt`:

```
echo "$1 <$1@example.com>"
```

Again, this will only generate a dummy name and email based on the SVN username, so feel free to alter it if you can provide a more meaningful mapping.

### Fetch the new SVN commits

Unlike SVN, Git makes a distinction between _downloading_ upstream commits and _integrating_ them into the project. The former is called "fetching", while the latter can be done via merging or rebasing. In the `~/GitMigration` directory, run the following command to fetch any new commits from the original SVN repository.

```
git svn fetch
```

This is similar to the `git svn clone` command from the previous phase in that it only updates the Git repository’s remote branches--the local branches will not reflect any of the updates yet. Your remote branches, on the other hand, should exactly match your SVN repo’s history.

If you’re using the `--authors-prog` option, you need include it in the above command, like so:

```
git svn fetch --authors-prog=authors.sh
```

### Synchronize with the fetched commits

To apply the downloaded commits to the repository, run the following command:

```
java -Dfile.encoding=utf-8 -jar ~/svn-migration-scripts.jar sync-rebase
```

This will rebase the fetched commits onto your local branches so that they match their remote counterparts. You should now be able to see the new commits in your `git log` output.

### Clean up the Git repo (again)

It’s also a good idea to run the `git-clean` script again to remove any obsolete tags or branches that were deleted from the original SVN repository since the last sync:

```
java -Dfile.encoding=utf-8 -jar ~/svn-migration-scripts.jar clean-git 
 --force
```

Your local Git repository should now be synchronized with your SVN repository.

### Summary

During this transition period, it’s very important that your developers only commit to the original SVN repository. The only time the Git repository should be updated is via the synchronization process discussed above. This is much easier than managing a two-way synchronization workflow, but it still allows you to start integrating Git into your build process.
