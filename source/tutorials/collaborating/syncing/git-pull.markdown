Merging upstream changes into your local repository is a common task in Git-based collaboration workflows. We already know how to do this with [`git fetch`](/tutorials/syncing/git-fetch) followed by [`git merge`](/tutorials/using-branches/git-merge), but `git pull` rolls this into a single command.

### Usage

```
git pull <remote>
```

Fetch the specified remote’s copy of the current branch and immediately merge it into the local copy. This is the same as `git fetch <remote>` followed by `git merge origin/<current-branch>.`

```
git pull --rebase <remote>
```

Same as the above command, but instead of using `git merge` to integrate the remote branch with the local one, use `git rebase`.

### Discussion

You can think of `git pull` as Git's version of `svn update`. It’s an easy way to synchronize your local repository with upstream changes. The following diagram explains each step of the pulling process.

![Git Tutorial: git pull](/images/tutorials/collaborating/syncing/03.svg)

You start out thinking your repository is synchronized, but then `git fetch` reveals that origin's version of `master` has progressed since you last checked it. Then `git merge` immediately integrates the `remote master` into the local one:

#### Pulling via Rebase

The `--rebase` option can be used to ensure a linear history by preventing unnecessary merge commits. Many developers prefer rebasing over merging, since it’s like saying, "I want to put my changes on top of what everybody else has done." In this sense, using `git pull` with the `--rebase` flag is even more like svn update than a plain `git pull`.

In fact, pulling with `--rebase` is such a common workflow that there is a dedicated configuration option for it:

```
git config --global branch.autosetuprebase always
```

After running that command, all `git pull` commands will integrate via `git rebase` instead of `git merge`.

### Examples

The following example demonstrates how to synchronize with the central repository's `master branch`:

```
git checkout master
git pull --rebase origin
```

This simply moves your local changes onto the top of what everybody else has already contributed.
