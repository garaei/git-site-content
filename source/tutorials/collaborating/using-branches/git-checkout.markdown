The `git checkout` command lets you navigate between the branches created by `git branch`. Checking out a branch updates the files in the working directory to match the version stored in that branch, and it tells Git to record all new commits on that branch. Think of it as a way to select which line of development you’re working on.

In the [previous module](/tutorials/undoing-changes), we saw how `git checkout` can be used to view old commits. Checking out branches is similar in that the working directory is updated to match the selected branch/revision; however, new changes _are_ saved in the project history—that is, it’s not a read-only operation.

### Usage

```
git checkout <existing-branch>
```

Check out the specified branch, which should have already been created with `git branch`. This makes <existing-branch> the current branch, and updates the working directory to match.

```
git checkout -b <new-branch>
```

Create and check out <new-branch>. The `-b` option is a convenience flag that tells Git to run `git branch <new-branch>` before running `git checkout <new-branch>`. `git checkout -b <new-branch> <existing-branch>`

Same as the above invocation, but base the new branch off of <existing-branch> instead of the current branch.

### Discussion

`git checkout` works hand-in-hand with `git branch`. When you want to start a new feature, you create a branch with `git branch`, then check it out with `git checkout`. You can work on multiple features in a single repository by switching between them with `git checkout`.

![Git Tutorial: Switch between multiple features in a single repo with git checkout.](/images/tutorials/collaborating/using-branches/04.svg)

Having a dedicated branch for each new feature is a dramatic shift from the traditional SVN workflow. It makes it ridiculously easy to try new experiments without the fear of destroying existing functionality, and it makes it possible to work on many unrelated features at the same time. In addition, branches also facilitate several collaborative workflows.

#### Detached `HEAD`s

Now that we’ve seen the three main uses of `git checkout` we can talk about that “detached `HEAD`” we encountered in the previous module.

Remember that the `HEAD` is Git’s way of referring to the current snapshot. Internally, the `git checkout` command simply updates the `HEAD` to point to either the specified branch or commit. When it points to a branch, Git doesn't complain, but when you check out a commit, it switches into a “detached `HEAD`” state.

![Git Tutorial: Attached vs Detached Head](/images/tutorials/collaborating/using-branches/05.svg)

This is a warning telling you that everything you’re doing is “detached” from the rest of your project’s development. If you were to start developing a feature while in a detached `HEAD` state, there would be no branch allowing you to get back to it. When you inevitably check out another branch (e.g., to merge your feature in), there would be no way to reference your feature:

![Git Tutorial: Detached Head state](/images/tutorials/collaborating/using-branches/06.svg)

The point is, your development should always take place on a branch—never on a detached `HEAD`. This makes sure you always have a reference to your new commits. However, if you’re just looking at an old commit, it doesn’t really matter if you’re in a detached `HEAD` state or not.

### Example

The following example demonstrates the basic Git branching process. When you want to start working on a new feature, you create a dedicated branch and switch into it:

```
git branch new-feature
git checkout new-feature
```

Then, you can commit new snapshots just like we’ve seen in previous modules:

```
# Edit some files
git add <file>
git commit -m "Started work on a new feature"
# Repeat
```

All of these are recorded in `new-feature`, which is completely isolated from `master`. You can add as many commits here as necessary without worrying about what’s going on in the rest of your branches. When it’s time to get back to “official” code base, simply check out the `master` branch:

```
git checkout master
```

This shows you the state of the repository before you started your feature. From here, you have the option to merge in the completed feature, branch off a brand new, unrelated feature, or do some work with the stable version of your project.
