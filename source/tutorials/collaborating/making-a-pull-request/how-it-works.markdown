Pull requests can be used in conjunction with the [Feature Branch Workflow](/tutorials/comparing-workflows/feature-branch-workflow), the [Gitflow Workflow](/tutorials/comparing-workflows/gitflow-workflow), or the [Forking Workflow](/tutorials/comparing-workflows/forking-workflow). But a pull request requires either two distinct branches or two distinct repositories, so they will not work with the [Centralized Workflow](/tutorials/comparing-workflows/centralized-workflow). Using pull requests with each of these workflows is slightly different, but the general process is as follows:

1. A developer creates the feature in a dedicated branch in their local repo.

2. The developer pushes the branch to a public Bitbucket repository.

3. The developer files a pull request via Bitbucket.

4. The rest of the team reviews the code, discusses it, and alters it.

5. The project maintainer merges the feature into the official repository and closes the pull request. 

The rest of this section describes how pull requests can be leveraged against different collaboration workflows.

### Feature Branch Workflow With Pull Requests

The Feature Branch Workflow uses a shared Bitbucket repository for managing collaboration, and developers create features in isolated branches. But, instead of immediately merging them into `master`, developers should open a pull request to initiate a discussion around the feature before it gets integrated into the main codebase.

![Pull Request: Feature Branch workflow](/images/tutorials/collaborating/making-a-pull-request/04.svg)

There is only one public repository in the Feature Branch Workflow, so the pull request’s destination repository and the source repository will always be the same. Typically, the developer will specify their feature branch as the source branch and the `master` branch as the destination branch.

After receiving the pull request, the project maintainer has to decide what to do. If the feature is ready to go, they can simply merge it into `master` and close the pull request. But, if there are problems with the proposed changes, they can post feedback in the pull request. Follow-up commits will show up right next to the relevant comments.

It’s also possible to file a pull request for a feature that is incomplete. For example, if a developer is having trouble implementing a particular requirement, they can file a pull request containing their work-in-progress. Other developers can then provide suggestions inside of the pull request, or even fix the problem themselves with additional commits.

### Gitflow Workflow With Pull Requests

The Gitflow Workflow is similar to the Feature Branch Workflow, but defines a strict branching model designed around the project release. Adding pull requests to the Gitflow Workflow gives developers a convenient place to talk about a release branch or a maintenance branch while they’re working on it.

![Pull Requests: Gitflow Workflow](/images/tutorials/collaborating/making-a-pull-request/05.svg)
![Pull Requests: Gitflow Workflow 2](/images/tutorials/collaborating/making-a-pull-request/06.svg)

The mechanics of pull requests in the Gitflow Workflow are the exact same as the previous section: a developer simply files a pull request when a feature, release, or hotfix branch needs to be reviewed, and the rest of the team will be notified via Bitbucket.

Features are generally merged into the `develop` branch, while release and hotfix branches are merged into both `develop` and `master`. Pull requests can be used to formally manage all of these merges.

### Forking Workflow With Pull Requests

In the Forking Workflow, a developer pushes a completed feature to _their own_ public repository instead of a shared one. After that, they file a pull request to let the project maintainer know that it’s ready for review.

The notification aspect of pull requests is particularly useful in this workflow because the project maintainer has no way of knowing when another developer has added commits to their Bitbucket repository.

![Pull Requests: Forking workflow](/images/tutorials/collaborating/making-a-pull-request/07.svg)

Since each developer has their own public repository, the pull request’s source repository will differ from its destination repository. The source repository is the developer’s public repository and the source branch is the one that contains the proposed changes. If the developer is trying to merge the feature into the main codebase, then the destination repository is the official project and the destination branch is `master`.

Pull requests can also be used to collaborate with other developers outside of the official project. For example, if a developer was working on a feature with a teammate, they could file a pull request using the _teammate’s_ Bitbucket repository for the destination instead of the official project. They would then use the same feature branch for the source and destination branches.

![Pull Requests: Forking workflow](/images/tutorials/collaborating/making-a-pull-request/08.svg)

The two developers could discuss and develop the feature inside of the pull request. When they’re done, one of them would file another pull request asking to merge the feature into the official master branch. This kind of flexibility makes pull requests very powerful collaboration tool in the Forking workflow.
