Pull requests are a feature that makes it easier for developers to collaborate using [Bitbucket](http://www.bitbucket.org). They provide a user-friendly web interface for discussing proposed changes before integrating them into the official project.

![Git Workflows: Pull Request in Bitbucket](/images/tutorials/collaborating/making-a-pull-request/01.svg)

In their simplest form, pull requests are a mechanism for a developer to notify team members that they have completed a feature. Once their feature branch is ready, the developer files a pull request via their Bitbucket account. This lets everybody involved know that they need to review the code and merge it into the `master` branch.

But, the pull request is more than just a notification—it’s a dedicated forum for discussing the proposed feature. If there are any problems with the changes, teammates can post feedback in the pull request and even tweak the feature by pushing follow-up commits. All of this activity is tracked directly inside of the pull request.

![Git Workflows: Activity inside a pull request](/images/tutorials/collaborating/making-a-pull-request/02.svg)

Compared to other collaboration models, this formal solution for sharing commits makes for a much more streamlined workflow. SVN and Git can both automatically send notification emails with a simple script; however, when it comes to discussing changes, developers typically have to rely on email threads. This can become haphazard, especially when follow-up commits are involved. Pull requests put all of this functionality into a friendly web interface right next to your Bitbucket repositories.

### Anatomy of a Pull Request

When you file a pull request, all you’re doing is _requesting_ that another developer (e.g., the project maintainer) _pulls_ a branch from your repository into their repository. This means that you need to provide 4 pieces of information to file a pull request: the source repository, the source branch, the destination repository, and the destination branch.

![Git Workflows: Pull Requests](/images/tutorials/collaborating/making-a-pull-request/03.svg)

Many of these values will be set to a sensible default by Bitbucket. However, depending on your collaboration workflow, your team may need to specify different values. The above diagram shows a pull request that asks to merge a feature branch into the official master branch, but there are many other ways to use pull requests.
