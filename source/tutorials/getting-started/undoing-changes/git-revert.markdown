The `git revert` command undoes a committed snapshot. But, instead of removing the commit from the project history, it figures out how to undo the changes introduced by the commit and appends a _new_ commit with the resulting content. This prevents Git from losing history, which is important for the integrity of your revision history and for reliable collaboration.

![Git Tutorial: git revert](/images/tutorials/getting-started/undoing-changes/03.svg)

### Usage

```
git revert <commit>
```

Generate a new commit that undoes all of the changes introduced in `<commit>`, then apply it to the current branch.

### Discussion

Reverting should be used when you want to remove an entire commit from your project history. This can be useful, for example, if you’re tracking down a bug and find that it was introduced by a single commit. Instead of manually going in, fixing it, and committing a new snapshot, you can use `git revert` to automatically do all of this for you.

#### Reverting vs. Resetting

It's important to understand that `git revert` undoes a single commit—it does not "revert" back to the previous state of a project by removing all subsequent commits. In Git, this is actually called a [`reset`](/tutorials/undoing-changes/git-reset), not a `revert`.

![Git Tutorial: Revert vs Reset](/images/tutorials/getting-started/undoing-changes/04.svg)

Reverting has two important advantages over resetting. First, it doesn’t change the project history, which makes it a “safe” operation for commits that have already been published to a shared repository. For details about why altering shared history is dangerous, please see the [`git reset`](/tutorials/undoing-changes/git-reset) page.

Second, `git revert` is able to target an individual commit at an arbitrary point in the history, whereas `git reset` can only work backwards from the current commit. For example, if you wanted to undo an old commit with `git reset`, you would have to remove all of the commits that occurred after the target commit, remove it, then re-commit all of the subsequent commits. Needless to say, this is not an elegant undo solution.

### Example

The following example is a simple demonstration of `git revert`. It commits a snapshot, then immediately undoes it with a revert.

```
# Edit some tracked files

# Commit a snapshot
git commit -m "Make some changes that will be undone"

# Revert the commit we just created
git revert HEAD
```

This can be visualized as the following:

![Git Tutorial: git revert Example](/images/tutorials/getting-started/undoing-changes/05.svg)

Note that the 4th commit is still in the project history after the revert. Instead of deleting it, `git revert` added a new commit to undo its changes. As a result, the 3rd and 5th commits represent the exact same code base, and the 4th commit is still in our history just in case we want to go back to it down the road.
