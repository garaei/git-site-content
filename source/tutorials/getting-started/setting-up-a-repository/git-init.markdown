The `git init` command creates a new Git repository. It can be used to convert an existing, unversioned project to a Git repository or initialize a new empty repository. Most of the other Git commands are not available outside of an initialized repository, so this is usually the first command you’ll run in a new project.

Executing `git init` creates a `.git` subdirectory in the project root, which contains all of the necessary metadata for the repo. Aside from the `.git` directory, an existing project remains unaltered (unlike SVN, Git doesn't require a `.git` folder in every subdirectory).

### Usage

```
git init
```

Transform the current directory into a Git repository. This adds a `.git` folder to the current directory and makes it possible to start recording revisions of the project.

```
git init <directory>
```

Create an empty Git repository in the specified directory. Running this command will create a new folder called `<directory ` containing nothing but the `.git` subdirectory.

```
git init --bare <directory>
```

Initialize an empty Git repository, but omit the working directory. Shared repositories should always be created with the `--bare` flag (see discussion below). Conventionally, repositories initialized with the `--bare` flag end in `.git`. For example, the bare version of a repository called `my-project` should be stored in a directory called `my-project.git`.

### Discussion

Compared to SVN, the `git init` command is an incredibly easy way to create new version-controlled projects. Git doesn’t require you to create a repository, import files, and check out a working copy. All you have to do is cd into your project folder and run `git init`, and you’ll have a fully functional Git repository.

However, for most projects, `git init` only needs to be executed once to create a central repository—developers typically don't use git init to create their local repositories. Instead, they'll usually use `git clone` to copy an existing repository onto their local machine.

#### Bare Repositories

The `--bare` flag creates a repository that doesn’t have a working directory, making it impossible to edit files and commit changes in that repository. Central repositories should always be created as bare repositories because pushing branches to a non-bare repository has the potential to overwrite changes. Think of `--bare` as a way to mark a repository as a storage facility, opposed to a development environment. This means that for virtually all Git workflows, the central repository is bare, and developers local repositories are non-bare.

![Git Tutorial: Bare Repositories](/images/tutorials/getting-started/setting-up-a-repository/01.svg)

### Example

Since `git clone` is a more convenient way to create local copies of a project, the most common use case for `git init` is to create a central repository:

```
ssh <user>@<host>
cd path/above/repo 
git init --bare my-project.git
```

First, you SSH into the server that will contain your central repository. Then, you navigate to wherever you’d like to store the project. Finally, you use the `--bare` flag to create a central storage repository. Developers would then `[clone](/tutorials/setting-up-a-repository/git-clone) my-project.git` to create a local copy on their development machine.
