Rebasing is the process of moving a branch to a new base commit. The general process can be visualized as the following:

![Git Tutorial: Rebase to maintain a linear project history.](/images/tutorials/getting-started/rewriting-history/02.svg)

From a content perspective, rebasing really is just moving a branch from one commit to another. But internally, Git accomplishes this by creating new commits and applying them to the specified base—it’s literally rewriting your project history. It’s very important to understand that, even though the branch looks the same, it’s composed of entirely new commits.

### Usage

```
git rebase <base>
```

Rebase the current branch onto <base>, which can be any kind of commit reference (an ID, a branch name, a tag, or a relative reference to `HEAD`).

### Discussion

The primary reason for rebasing is to maintain a linear project history. For example, consider a situation where the master branch has progressed since you started working on a feature:

![Git Rebase Branch onto Master](/images/tutorials/getting-started/rewriting-history/03.svg)

You have two options for integrating your feature into the `master` branch: merging directly or rebasing and then merging. The former option results in a 3-way merge and a merge commit, while the latter results in a fast-forward merge and a perfectly linear history. The following diagram demonstrates how rebasing onto `master` facilitates a fast-forward merge.

![Git Tutorial: Fast-forward merge](/images/tutorials/getting-started/rewriting-history/04.svg)

Rebasing is a common way to integrate upstream changes into your local repository. Pulling in upstream changes with `git merge` results in a superfluous merge commit every time you want to see how the project has progressed. On the other hand, rebasing is like saying, “I want to base my changes on what everybody has already done.”

#### Don’t Rebase Public History

As we’ve discussed with `git commit --amend` and `git reset`, you should never rebase commits that have been pushed to a public repository. The rebase would replace the old commits with new ones, and it would look like that part of your project history abruptly vanished.

### Examples

The example below combines git rebase with git merge to maintain a linear project history. This is a quick and easy way to ensure that your merges will be fast-forwarded.

```
# Start a new feature
git checkout -b new-feature master
# Edit files
git commit -a -m "Start developing a feature"
```

In the middle of our feature, we realize there’s a security hole in our project

```
# Create a hotfix branch based off of master
git checkout -b hotfix master
# Edit files
git commit -a -m "Fix security hole"
# Merge back into master
git checkout master
git merge hotfix
git branch -d hotfix
```

After merging the hotfix into master, we have a forked project history. Instead of a plain git merge, we’ll integrate the feature branch with a rebase to maintain a linear history:

```
git checkout new-feature
git rebase master
```

This moves new-feature to the tip of master, which lets us do a standard fast-forward merge from master:

```
git checkout master
git merge new-feature
```
