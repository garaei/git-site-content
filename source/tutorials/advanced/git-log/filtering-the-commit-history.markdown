Formatting how each commit gets displayed is only half the battle of learning `git log`. The other half is understanding how to navigate the commit history. The rest of this article introduces some of the advanced ways to pick out specific commits in your project history using `git log`. All of these can be combined with any of the formatting options discussed above.

### By Amount

The most basic filtering option for `git log` is to limit the number of commits that are displayed. When you’re only interested in the last few commits, this saves you the trouble of viewing all the commits in a pager.

You can limit `git log`’s output by including the `-<n>` option. For example, the following command will display only the 3 most recent commits.

```
git log -3
```

### By Date

If you’re looking for a commit from a specific time frame, you can use the `--after` or `--before` flags for filtering commits by date. These both accept a variety of date formats as a parameter. For example, the following command only shows commits that were created _after_ July 1st, 2014 (inclusive):

```
git log --after="2014-7-1"
```

You can also pass in relative references like `"1 week ago"` and `"yesterday"`:

```
get log --after="yesterday"
```

To search for a commits that were created between two dates, you can provide both a `--before` and `--after` date. For instance, to display all the commits added between July 1st, 2014 and July 4th, 2014, you would use the following:

```
git log --after="2014-7-1" --before="2014-7-4"
```

Note that the `--since` and `--until` flags are synonymous with `--after` and `--before`, respectively.

### By Author

When you’re only looking for commits created by a particular user, use the `--author` flag. This accepts a regular expression, and returns all commits whose author matches that pattern. If you know exactly who you’re looking for, you can use a plain old string instead of a regular expression:

```
git log --author="John"
```

This displays all commits whose author includes the name _John_.  The author name doesn’t need to be an _exact_ match—it just needs to _contain_ the specified phrase.

You can also use regular expressions to create more complex searches. For example, the following command searches for commits by either _Mary_ or _John_.

```
git log --author="John\|Mary"
```

Note that the author’s email is also included with the author’s name, so you can use this option to search by email, too.

If your workflow separates committers from authors, the `--committer` flag operates in the same fashion.

### By Message

To filter commits by their commit message, use the `--grep` flag. This works just like the `--author` flag discussed above, but it matches against the commit message instead of the author.

For example, if your team includes relevant issue numbers in each commit message, you can use something like the following to pull out all of the commits related to that issue:

```
git log --grep="JRA-224:"
```

You can also pass in the `-i` parameter to `git log` to make it ignore case differences while pattern matching.

### By File

Many times, you’re only interested in changes that happened to a particular file. To show the history related to a file, all you have to do is pass in the file path. For example, the following returns all commits that affected either the `foo.py` or the `bar.py` file:

```
git log -- foo.py bar.py
```

The `--` parameter is used to tell `git log` that subsequent arguments are file paths and not branch names. If there’s no chance of mixing it up with a branch, you can omit the `--`.

### By Content

It’s also possible to search for commits that introduce or remove a particular line of source code. This is called a _pickaxe_, and it takes the form of `-S"<string>"`. For example, if you want to know when the string _Hello, World!_ was added to any file in the project, you would use the following command:

```
git log -S"Hello, World!"
```

If you want to search using a regular expression instead of a string, you can use the `-G"<regex>"` flag instead.

This is a very powerful debugging tool, as it lets you locate all of the commits that affect a particular line of code. It can even show you when a line was copied or moved to another file.

### By Range

You can pass a range of commits to `git log` to show only the commits contained in that range. The range is specified in the following format, where `<since>` and `<until>` are commit references:

```
git log <since>..<until>
```

This command is particularly useful when you use branch references as the parameters. It’s a simple way to show the differences between 2 branches. Consider the following command:

```
git log master..feature
```

The `master..feature` range contains all of the commits that are in the `feature` branch, but aren’t in the `master` branch. In other words, this is how far `feature` has progressed since it forked off of `master`. You can visualize this as follows:

![Detecting a fork in the history using ranges](/images/tutorials/advanced/git-log/01.svg)

Note that if you switch the order of the range (`feature..master`), you will get all of the commits in `master`, but not in `feature`. If `git log` outputs commits for both versions, this tells you that your history has diverged.

### Filtering Merge Commits

By default, `git log` includes merge commits in its output. But, if your team has an always-merge policy (that is, you merge upstream changes into topic branches instead of rebasing the topic branch onto the upstream branch), you’ll have a lot of extraneous merge commits in your project history.

You can prevent `git log` from displaying these merge commits by passing the `--no-merges` flag:

```
git log --no-merges
```

On the other hand, if you’re _only_ interested in the merge commits, you can use the `--merges` flag:

```
git log --merges
```

This returns all commits that have at least two parents.
