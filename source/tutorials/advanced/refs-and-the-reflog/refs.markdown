A **ref** is an indirect way of referring to a commit. You can think of it as a user-friendly alias for a commit hash. This is Git’s internal mechanism of representing branches and tags.

Refs are stored as normal text files in the `.git/refs` directory, where `.git` is usually called `.git`. To explore the refs in one of your repositories, navigate to `.git/refs`. You should see the following structure, but it will contain different files depending on what branches, tags, and remotes you have in your repo:

```
.git/refs/
    heads/
        master
        some-feature
    remotes/
        origin/
            master
    tags/
        v0.9
```

The `heads` directory defines all of the local branches in you repository. Each filename matches the name of the corresponding branch, and inside the file you’ll find a commit hash. This commit hash is the location of the tip of the branch. To verify this, try running the following two commands from the root of the Git repository:

```
# Output the contents of `refs/heads/master` file:
cat .git/refs/heads/master

# Inspect the commit at the tip of the `master` branch:
git log -1 master
```

The commit hash return by the `cat` command should match the commit ID displayed by `git log`.

To change the location of the `master` branch, all Git has to do is change the contents of the `refs/heads/master` file. Similarly, creating a new branch is simply a matter of writing a commit hash to a new file. This is part of the reason why Git branches are so lightweight compared to SVN.

The `tags` directory works the exact same way, but it contains tags instead of branches. The `remotes` directory lists all remote repositories that you created with `git remote` as separate subdirectories. Inside each one, you’ll find all the remote branches that have been fetched into your repository.

### Specifying Refs

When passing a ref to a Git command, you can either define the full name of the ref, or use a short name and let Git search for a matching ref. You should already be familiar with short names for refs, as this is what you’re using each time you refer to a branch by name.

```
git show some-feature
```

The `some-feature` argument in the above command is actually a short name for the branch. Git resolves this to `refs/heads/some-feature` before using it. You can also specify the full ref on the command line, like so:

```
git show refs/heads/some-feature
```

This avoids any ambiguity regarding the location of the ref. This is necessary, for instance, if you had both a tag and a branch called `some-feature`. However, if you’re using proper naming conventions, ambiguity between tags and branches shouldn’t generally be a problem.

We’ll see more full ref names in the Refspecs section.