The most direct way to reference a commit is via its SHA-1 hash. This acts as the unique ID for each commit. You can find the hash of all your commits in the `git log` output.

```
commit 0c708fdec272bc4446c6cabea4f0022c2b616eba
Author: Mary Johnson <mary@example.com>
Date:   Wed Jul 9 16:37:42 2014 -0500

    Some commit message
```

When passing the commit to other Git commands, you only need to specify enough characters to uniquely identify the commit. For example, you can inspect the above commit with `git show` by running the following command:

```
git show 0c708f
```

It’s sometimes necessary to resolve a branch, tag, or another indirect reference into the corresponding commit hash. For this, you can use the `git rev-parse` command. The following returns the hash of the commit pointed to by the `master` branch:

```
git rev-parse master
```

This is particularly useful when writing custom scripts that accept a commit reference. Instead of parsing the commit reference manually, you can let `git rev-parse` normalize the input for you.