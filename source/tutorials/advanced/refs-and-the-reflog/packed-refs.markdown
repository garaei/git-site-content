For large repositories, Git will periodically perform a garbage collection to remove unnecessary objects and compress refs into a single file for more efficient performance. You can force this compression with the garbage collection command:

```
git gc
```

This moves all of the individual branch and tag files in the `refs` folder into a single file called `packed-refs` located in the top of the `.git` directory. If you open up this file, you’ll find a mapping of commit hashes to refs:

```
00f54250cf4e549fdfcafe2cf9a2c90bc3800285 refs/heads/feature
0e25143693cfe9d5c2e83944bbaf6d3c4505eb17 refs/heads/master
bb883e4c91c870b5fed88fd36696e752fb6cf8e6 refs/tags/v0.9
```

On the outside, normal Git functionality won’t be affected in any way. But, if you’re wondering why your `.git/refs` folder is empty, this is where the refs went.