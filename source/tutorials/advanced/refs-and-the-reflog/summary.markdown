You should now be quite comfortable referring to commits in a Git
repository. We learned how branches and tags were stored as refs in the
`.git` subdirectory, how to read a `packed-refs`
file, how `HEAD` is represented, how to use refspecs for advanced
pushing and fetching, and how to use the relative `~` and
`^` operators to traverse a branch hierarchy.

We also took a look at the reflog, which is a way to reference commits that
are not available through any other means. This is a great way to recover from
those little “Oops, I shouldn’t have done that”
situations.

The point of all this was to be able to pick out exactly the commit that you
need in any given development scenario. It’s very easy to leverage the
skills you learned in this article against your existing Git knowledge, as some
of the most common commands accept refs as arguments, including `git
log`, `git show`, `git checkout`, `git
reset`, `git revert`, `git rebase`, and many
others.