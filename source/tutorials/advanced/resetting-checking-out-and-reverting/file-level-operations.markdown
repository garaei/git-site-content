The `git reset` and `git checkout` commands also
accept an optional file path as a parameter. This dramatically alters their
behavior. Instead of operating on entire snapshots, this forces them to limit
their operations to a single file.

### Reset

When invoked with a file path, `git reset` updates the
_staged snapshot_ to match the version from the specified commit. For
example, this command will fetch the version of `foo.py` in the
2nd-to-last commit and stage it for the next commit:

```
git reset HEAD~2 foo.py
```

As with the commit-level version of `git reset`, this is more
commonly used with `HEAD` rather than an arbitrary commit.  Running
`git reset HEAD foo.py` will unstage `foo.py`. The
changes it contains will still be present in the working directory.

![Moving a file from the commit history into the staged snapshot](/images/tutorials/advanced/resetting-checking-out-and-reverting/07.svg)

The `--soft`, `--mixed`, and `--hard` flags
do not have any effect on the file-level version of `git reset`, as
the staged snapshot is _always_ updated, and the working directory is
_never_ updated.

### Checkout

Checking out a file is similar to using `git reset` with a file
path, except it updates the _working directory_ instead of the stage.
Unlike the commit-level version of this command, this does not move the
`HEAD` reference, which means that you won’t switch
branches.

![Moving a file from the commit history into the working directory](/images/tutorials/advanced/resetting-checking-out-and-reverting/08.svg)

For example, the following command makes `foo.py` in the working
directory match the one from the 2nd-to-last commit:

```
git checkout HEAD~2 foo.py
```

Just like the commit-level invocation of `git checkout`, this can
be used to inspect old versions of a project—but the scope is limited to
the specified file.

If you stage and commit the checked-out file, this has the effect of
“reverting” to the old version of that file. Note that this removes
_all_ of the subsequent changes to the file, whereas the `git
revert` command undoes only the changes introduced by the specified
commit.

Like `git reset`, this is commonly used with `HEAD` as
the commit reference. For instance, `git checkout HEAD foo.py` has
the effect of discarding unstaged changes to `foo.py`. This is
similar behavior to `git reset HEAD --hard`, but it operates only on
the specified file.