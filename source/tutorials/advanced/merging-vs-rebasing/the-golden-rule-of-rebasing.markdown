Once you understand what rebasing is, the most important thing to learn is when _not_ to do it. The golden rule of `git rebase` is to never use it on _public_ branches.

For example, think about what would happen if you rebased `master` onto your `feature` branch:

![Rebasing the master branch](/images/tutorials/advanced/merging-vs-rebasing/05.svg)

The rebase moves all of the commits in `master` onto the tip of `feature`. The problem is that this only happened in _your_ repository. All of the other developers are still working with the original `master`. Since rebasing results in brand new commits, Git will think that your `master` branch’s history has diverged from everybody else’s.

The only way to synchronize the two `master` branches is to merge them back together, resulting in an extra merge commit _and_ two sets of commits that contain the same changes (the original ones, and the ones from your rebased branch). Needless to say, this is a very confusing situation.

So, before you run `git rebase`, always ask yourself, “Is anyone else looking at this branch?” If the answer is yes, take your hands off the keyboard and start thinking about a non-destructive way to make your changes (e.g., the `git revert` command). Otherwise, you’re safe to re-write history as much as you like.

### Force-Pushing

If you try to push the rebased `master` branch back to a remote repository, Git will prevent you from doing so because it conflicts with the remote `master` branch. But, you can force the push to go through by passing the `--force` flag, like so:

```
# Be very careful with this command!
git push --force
```

This overwrites the remote `master` branch to match the rebased one from your repository and makes things very confusing for the rest of your team. So, be very careful to use this command only when you know exactly what you’re doing.

One of the only times you should be force-pushing is when you’ve performed a local cleanup _after_ you’ve pushed a private feature branch to a remote repository (e.g., for backup purposes). This is like saying, “Oops, I didn’t really want to push that original version of the feature branch. Take the current one instead.” Again, it’s important that nobody is working off of the commits from the original version of the feature branch.
