All Git hooks are ordinary scripts that Git executes when certain events occur in the repository. This makes them very easy to install and configure.

Hooks can reside in either local or server-side repositories, and they are only executed in response to actions in that repository. We’ll take a concrete look at categories of hooks later in this article. The configuration discussed in the rest of this section apply to both local and server-side hooks.

### Installing Hooks

Hooks reside in the `.git/hooks` directory of every Git repository. Git automatically populates this directory with example scripts when you initialize a repository. If you take a look inside `.git/hooks`, you’ll find the following files:

```
applypatch-msg.sample       pre-push.sample
commit-msg.sample           pre-rebase.sample
post-update.sample          prepare-commit-msg.sample
pre-applypatch.sample       update.sample
pre-commit.sample
```

These represent most of the available hooks, but the `.sample` extension prevents them from executing by default. To “install” a hook, all you have to do is remove the `.sample` extension. Or, if you’re writing a new script from scratch, you can simply add a new file matching one of the above filenames, minus the `.sample` extension.

As an example, try installing a simple `prepare-commit-msg` hook. Remove the `.sample` extension from this script, and add the following to the file:

```
#!/bin/sh

echo "# Please include a useful commit message!" > $1
```

Hooks need to be executable, so you may need to change the file permissions of the script if you’re creating it from scratch.  For example, to make sure that `prepare-commit-msg` is executable, you would run the following command:

```
chmod +x prepare-commit-msg
```

You should now see this message in place of the default commit message every time you run `git commit`. We’ll take a closer look at how this actually works in the Prepare Commit Message section. For now, let’s just revel in the fact that we can customize some of Git’s internal functionality.

The built-in sample scripts are very useful references, as they document the parameters that are passed in to each hook (they vary from hook to hook).

### Scripting Languages

The built-in scripts are mostly shell and PERL scripts, but you can use any scripting language you like as long as it can be run as an executable. The shebang line (`#!/bin/sh`) in each script defines how your file should be interpreted. So, to use a different language, all you have to do is change it to the path of your interpreter.

For instance, we can write an executable Python script in the `prepare-commit-msg` file instead of using shell commands. The following hook will do the same thing as the shell script in the previous section.

```
#!/usr/bin/env python

import sys, os

commit_msg_filepath = sys.argv[1]
with open(commit_msg_filepath, 'w') as f:
    f.write("# Please include a useful commit message!")
```

Notice how the first line changed to point to the Python interpreter. And, instead of using `$1` to access the first argument passed to the script, we used `sys.argv[1]` (again, more on this in a moment).

This is a very powerful feature for Git hooks because it lets you work in whatever language you’re most comfortable with.

### Scope of Hooks

Hooks are local to any given Git repository, and they are _not_ copied over to the new repository when you run `git clone`. And, since hooks are local, they can be altered by anybody with access to the repository.

This has an important impact when configuring hooks for a team of developers. First, you need to find a way to make sure hooks stay up-to-date amongst your team members. Second, you can’t force developers to create commits that look a certain way—you can only encourage them to do so.

Maintaining hooks for a team of developers can be a little tricky because the `.git/hooks` directory isn’t cloned with the rest of your project, nor is it under version control. A simple solution to both of these problems is to store your hooks in the actual project directory (above the `.git` directory). This lets you edit them like any other version-controlled file. To install the hook, you can either create a symlink to it in `.git/hooks`, or you can simply copy and paste it into the `.git/hooks` directory whenever the hook is updated.

![Hooks executing during the commit creation process](/images/tutorials/advanced/git-hooks/02.svg)

As an alternative, Git also provides a [Template Directory](http://git-scm.com/docs/git-init#_template_directory) mechanism that makes it easier to install hooks automatically. All of the files and directories contained in this template directory are copied into the `.git` directory every time you use `git init` or `git clone`.

All of the [local hooks](/tutorials/git-hooks/local-hooks) described below can be altered—or completely un-installed—by the owner of a repository. It’s entirely up to each team member whether or not they actually use a hook. With this in mind, it’s best to think of Git hooks as a convenient developer tool rather than a strictly enforced development policy.

That said, it is possible to reject commits that do not conform to some standard using [server-side hooks](/tutorials/git-hooks/server-side-hooks). We’ll talk more about this later in the article.