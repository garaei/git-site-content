---
author: Nicola Paolucci
title: "Use Git even if your team doesn't: git-svn tips and tricks"
date: 2014-09-03
---

Before joining Atlassian, I'd been working on various projects that still used Subversion (SVN) as their version control system. I had moved to Git already years before, and I wanted to keep using it as much as possible.

Luckily I could use [git-svn][2]: An incredibly complete solution to interact with Subversion repositories without leaving the comfort of the Git power toolset. But there are gotchas. This post assumes you are already a little bit acquainted with git-svn, and you know how to interact with a SVN repository using it.

The list below contains all the tricks and tips I had to research and integrate in my workflow to keep using Git joyfully in conjunction with SVN. Enjoy!

### Setting up the files to ignore

You should make sure that `Git` ignores the same files `SVN` does. The simplest trick is to append the list of svn:ignore files to the default git exclude file:

```
git svn show-ignore >> .git/info/exclude
```

An alternative method is the magical `update-index`:

```
git update-index --assume-unchanged files to ignore
```

This is quite a fine trick, and I've consistently used in the past year. If you need more information, have a look at this [post on Stackoverflow][3]. If you use the latter method, how do you find out later that a file has been ignored in Git? You can use this:

```
git ls-files -v | grep ^[a-z] | sed -e 's/^h\ //'
```

**NOTE:** `update-index` is an advanced command that manipulates the index directly, **use it with care!**

### Shallow clone big repositories

I've worked with SVN codebases above 1.5Gb for a full checkout. In similar scenarios, checking out the entire history commit by commit – the way git-svn does – can be lengthy, as the repository is simply too big. The way around this issue is to create a shallow clone of the repository; instead of copying the entire history of the project you just copy the last n commits and proceed from there. For example:

```
git svn clone -s -r604:HEAD http://nick@svn.xxxxxx.com/repos/ -T trunk -b branches -t tags
```

Where "`604`" must be replaced with the earlier revision you want to keep. Mandatory [Stackoverflow reference][4].

### If you added the .svn folders by mistake in Git

Sooner or later you'll run into a few snags. For example one time I didn't use
git-svn, I just checked out a project from SVN but I wanted to do my own
tracking using Git. At that time I mistakenly added the `.svn` folders to the index (staging area) in `git`. How to keep those important files but
remove them from the index?  Tricky! Here's how to untrack files without
actually deleting them in `git`:

```
git status -s | grep .svn | awk {'print $3'} | xargs git rm --cached
```

The keyword here is `--cached` that operates on the index and not in the working directory. Very clearly explained in this [chapter on progit][5].

### What to do when a SVN repository moves

When a SVN repository moves (or when you have to access it via VPN and do some smart tunneling that will change its address) you have to follow the correct procedure to avoid a full re-checkout. The first method listed at the [git wiki][6] is the one with which I had consistent success. Directly from the wiki here is the important part:

*   Edit the `svn-remote` URL in `.git/config` to point to the new domain name or IP Address.
*   Run `git svn fetch` - _This needs to fetch at least one new revision from svn!_
*   Change `svn-remote` url back to the original url.
*   Run `git svn rebase -l` to do a local rebase (with the changes that came in with the last fetch operation).
*   Change `svn-remote` url back to the new url.
*   Run `git-svn rebase`. It should now work normally again!

This will only work if the `git-svn fetch` step actually fetches anything!

### Conclusion

Working with Git is a joy for me. You have total freedom and control, you can commit endlessly, reformat your commits, squash them into clean ones, and branch like crazy. You can bring the same joy with you even if you have to interact with SVN. I've done it for two years straight, and it works beautifully.

When your whole team is ready to migrate to Git, it's nice to have a comprehensive step-by-step guide. So we wrote one: 

[the Easy SVN to Git migration guide!][7]

[2]: http://www.kernel.org/pub/software/scm/git/docs/git-svn.html
[3]: http://stackoverflow.com/questions/655243/ignore-modified-but-not-committed-files-in-git
[4]: http://stackoverflow.com/questions/747075/how-to-git-svn-clone-the-last-n-revisions-from-a-subversion-repository
[5]: http://git-scm.com/book/en/Git-Basics-Recording-Changes-to-the-Repository#Removing-Files
[6]: https://git.wiki.kernel.org/index.php/GitSvnSwitch
[7]: /tutorials/migrating-overview