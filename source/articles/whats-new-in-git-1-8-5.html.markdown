---
author: Nicola Paolucci
title: "What's new in Git 1.8.5"
date: 2014-09-03
---

While we wait for the next major `git` release which will bring about some serious updates let's peel off the notes of the latest point release: `1.8.5` is upon us!

Here is what I could dig from the [release notes][10] that was interesting to me in my role of average `git` user.

Oh and if you haven't seen them yet check out my earlier notes on releases [1.8.2][7], [1.8.3][16] and [1.8.4][11].

### Customary installation pointers

It is customary for me to provide a few pointers for the upgrade, with caveats and reminders that package managers in some cases might delay the distribution; that is out of my hands. In any case, here are some painless instructions to upgrade:

- Type `brew update && brew upgrade git` if you use [homebrew][5] on OSX.
- Use a magic [apt trick][6] if on [Ubuntu][14] based distributions *(I leave other distributions as an exercise for the reader)*.
- Simply run the new [installer][8] if on Windows *(the Windows port sometimes lags a bit behind so please be patient if you can't find it right away)*.

### `HEAD` new alias is finally live

This had been cooking for a while and it's finally in: `HEAD` has a new alias, instead of typing four capital letters you can say "`@`" now, e.g. "`git log @`".

### Some `rebase` polish

Rebase saw some polish:

- In the operations sheet of the `--interactive` `rebase` the list of commit ids will now conform to the `core.abbrev` length (`core.abbrev` is the configuration that holds how long the abbreviated hashes of the commits should be, the default being `7`).

- You can now customize if `git pull --rebase` should use the default flattening or preserve merges by modifying setting `pull.rebase` to `preserve`. For more details on what this and `rebase --preserve-merges` do, please check [Stack Overflow's answer][9].

### Tiny `status` improvements

Varied improvements to `git status` have been included in this release:

- It no longer prints dirty status information for submodules for which submodule.$name.ignore is set to "all".

- During a `cherry-pick` it shows what original commit is being picked.

- It will not spit out its output prepended by `#` comment markers.  If you require scripts to parse the output of `git status` you should now update them to use `git status --porcelain`.

- `git status` and `git branch -v -v` now distinguish among a branch that does not build on any other branch, a branch that is in sync with the branch it builds on, and a branch that is configured to build on some other branch that no longer exists.

### Diff for **"all-but"** classes

`git diff --diff-filter` could already filter several classes of changes:

```
--diff-filter=[(A|C|D|M|R|T|U|X|B)...[*]]
    Select only files that are Added (A), Copied (C), Deleted (D), Modified (M), Renamed (R), have their type
    (i.e. regular file, symlink, submodule, ...) changed (T), are Unmerged (U), are Unknown (X), or have had
    their pairing Broken (B). Any combination of the filter characters (including none) can be used. When *
    (All-or-none) is added to the combination, all paths are selected if there is any file that matches other
    criteria in the comparison; if there is no file that matches other criteria, nothing is selected.
```

It can can now take lowercase letters to mean **show everything but these classes**.

### HTTP variables per site

- The "http.*" variables can now be specified per URL that the configuration applies.  For example the following would flip http.sslVerify off only when talking to that specified site:

```
[http]
    sslVerify = true
[http "https://weak.example.com/"]
    sslVerify = false
```

### Welcome submodule awareness on move

Submodule support has been steadily improving with every release, this time `git` gains the ability to relocate submodules:

```
'git mv A B' when moving a submodule A has been taught to relocate its working tree and to adjust the paths in the .gitmodules file.
```

### Performance, Internal Implementation, etc.

- The HTTP transport will try to use TCP keepalive when able.

- `git repack` is now written in C.

### Other tidbits

Other interesting tidbits directly from the release notes:

- `git whatchanged` deprecated but not removed. It's been made less prominent in the general part of the documentation.
- `git cherry-pick` now understands `git cherry-pick -` to pick from the previous branch.
- xdg-open can be used as a browser backend for `git web-browse` (hence to show `git help -w` output), when available.
- Just like `make -C <directory>`, `git -C <directory> ...` tells Git to go there before doing anything else.
- Make `foo^{tag}` to peel a tag to itself, i.e. no-op., and fail if "foo" is not a tag.  `git rev-parse --verify v1.0^{tag}` would be a more convenient way to say `test $(git cat-file -t v1.0) = tag`.
- `git push --no-thin` actually disables the "thin pack transfer" optimization.    
- Magic pathspecs like ":(icase)makefile" that matches both Makefile and makefile and ":(glob)foo/**/bar" that matches "bar" in "foo" and any subdirectory of "foo" can be used in more places.
- `git blame` can now take more than one -L option to discover the origin of multiple blocks of the lines.
- The http transport clients can optionally ask to save cookies with http.savecookies configuration variable.
- `git fetch` (hence "git pull" as well) learned to check `fetch.prune` and `remote.*.prune` configuration variables and to behave as if the `--prune` command line option was given.

That's it for now. Let me know if you find this update useful and stay tuned for more. Follow me [@durdn][3] and the awesome [@AtlDevtools][4] team for more DVCS rocking.

[1]: http://git-scm.com/
[2]: http://git-scm.com/about
[3]: http://twitter.com/durdn
[4]: http://twitter.com/AtlDevTools
[5]: http://mxcl.github.com/homebrew/
[6]: https://launchpad.net/~git-core/+archive/ppa
[7]: https://blogs.atlassian.com/2013/03/highlights-new-git-1-8-2/
[8]: http://msysgit.github.com/
[9]: http://stackoverflow.com/questions/15915430/what-exactly-does-gits-rebase-preserve-merges-do-and-why
[10]: https://github.com/git/git/blob/master/Documentation/RelNotes/1.8.5.txt
[11]: https://blogs.atlassian.com/2013/08/what-you-need-to-know-about-the-new-git-1-8-4/
[14]: http://www.ubuntu.com
[16]: https://blogs.atlassian.com/2013/05/highlights-git-1-8-3/


