---
author: Nicola Paolucci
title: "Git: Automatic Merges With Server Side Hooks (For The Win!)﻿"
date: 2014-09-03
---

Enterprise DVCS Workflows are settling and patterns are consolidating. The flexibility [git][1] gives teams is so broad that even within a single company different teams might use different approaches to code sharing and collaboration. 

I speak from hard evidence as this is exactly what happens at Atlassian. The [Bitbucket Server][5] team works differently than the [Confluence][8] team which works differently from the [JIRA][9] team. They all share a similar Agile process but have different approaches to branching, Continuous Integration and team organization.

Differences not withstanding, common patterns are emerging. A recurring paradigm in the industry is the use of a centralized repository with a *master* branch, several stable lines of development and follow a feature branch workflow where new features and bug fixes can be developed independently. This provides quick integration and allows teams to leverage the efficiency boost that comes with DVCS.

![bitbucket server team image][10]

If you read this and ponder a bit you might ask: if a company is following the centralized repository workflow listed above, why would they still need forks? 

There are several reasons why forking is useful and vital even within the enterprise. Before I present them let me first backtrack a bit and provide some context and definitions.

## Table of Contents

1. [What is a Fork?](#what-is-a)
2. [Forks have won the Open Source workflow choice award](#forks-have-won-the)
3. [Do you need Forks in the Enterprise?](#do-you-need-forks)
4. [Guard core components but encourage innovation and adoption](#guard-core)
5. [Organize cross department collaboration](#organize-cross)
6. [Reduce noise](#reduce)
7. [Streamline interaction with contractors](#streamline)
8. [Developer to developer interactions behind the firewall](#developer-to)
9. [Conclusions](#conclusions)

## What is a Fork?

In recent DVCS terminology a fork is a remote, server-side copy of a repository, distinct from the original. A clone is not a fork; a clone is a local copy of some remote repository. This differs slightly form the [general definition of forks in software development][11] but is the meaning that I will refer to in the course of this piece.

## Forks have won the Open Source workflow choice award

If you have been living under a rock for the past several years let me reveal to you an indisputable fact: *forks* have won the Open Source process choice award. They foster participation by drastically lowering the barrier of entry and the friction of code collaboration. 

Anybody can fork an open project and the original author does not incur in penalties or burdens because of it, the operation is transparent. He will *potentially* receive feedback or improvements in the form of [pull requests][14] but that's it.

## Do you need Forks in the Enterprise?

A fully distributed de-centralized approach is effective for loosely connected open source teams but what about enterprise teams all working in the same office with a central repository? Are forks useful in this setting? 

Forks are surprisingly useful also behind the firewalls of the enterprise.

In abstract terms *forks* can be used within an organization to **manage trust, track the maturity of codebases and facilitate cross team collaboration**.

Concrete examples include:

- Guard core components but encourage innovation and adoption.
- Organize Cross department collaboration.
- Reduce noise.
- Streamline interaction with contractors.
- Developer to Developer interactions behind the firewall.

Let's see in more details each one.

## Guard core components but encourage innovation and adoption

Many enterprises have core components that are reused across the company. Often these pieces have stricter policies regarding who can make changes, stability guarantees and heavier review process.

Forks allow to keep the sanctioned code closely guarded but at the same time encourage adoption and innovation. 

A non-sanctioned team or a lone developer with interest in the matter can fork the project and start contributing, without requiring supervision and without disrupting the core team's work. Collaboration still happens as usual with [pull requests][13] and feature branches.

By keeping experiments out of the main repositories one can effectively manage and track the maturity of contributions. Crazy unstable hacks remain sealed in their own forks; Solid pieces can be merged into the core components after they are reviewed. Why forks allow this and regular clones do not? Because forks allow the tracking bit: anybody can clone locally and work on their own experiments but pushing this code to a server-side fork allows it to be traceable.

Atlassian has several core libraries shared between the various groups, like for example the [Atlassian User Interface][13]. Forks relieve the burden on the Core teams maintaining them. No spurious feature branches and a messy tree appear in their main repository anymore.

## Organize cross department collaboration

An alternative but similar scenario is also common: when there is no "core" or ruling team over a certain software component but several departments maintain their own modified versions of the same piece of infrastructure. *Forks* allow teams to have control and guarantees on their own variations. Collaboration between departments is still easy and transparent with [pull requests][13] and the awesome integration features of [git][1].

## Reduce noise

When teams grow very large the noise of feature and bug fix branches can effectively become too much for UIs to properly display. Project history becomes so messy that is hard to understand what is going on and what is being merged where. The tools can become less effective in this situation. 

Again forks allow sub-teams to collaborate in the open but keep the central repositories where integrations happen as clean as possible.

## Streamline interaction with contractors

Another area where *forks* are valuable is in the interaction with third parties, contractors and freelances. By providing forks as the only access point for contractors to your repositories one can gain several benefits:

- Keep your main repository clean and restricted.
- Integrate third party work work after review at scheduled times.
- Retain the common git collaboration process.

## Developer to developer interactions behind the firewall

Let's not leave out one final and very important piece of the puzzle.

**Developer's personal forks!** A developer might be happy to hack, improve and enhance some piece of core infrastructure but he might not want to share his early work with the rest yet. Would you want him to push some mission critical proprietary code out in the open? 

In other cases he might want to maintain a slightly different approach to a problem and keep the contribution locked up until he can prove that a certain design decision will reap benefits for his team. For this scenario personal forks are great.

Personal forks allow also for the kind of distributed, uncoordinated kind of interactions that have made DVCS extremely successful in Open Source communities.

## Conclusions

Forks are not the answer to all code collaboration woes but offer an effective solution to several concerns. In this piece I presented some ideas that support the statement that *forks* inside the enterprise help manage trust, the maturity of code bases and facilitate cross team collaboration.

Let me conclude by shouting that Bitbucket Server's new release includes first class [support for forks][12] in addition to a whole lot of other features.

As usual ping me [@durdn][3] or the awesome team [@AtlDevtools][4] for more DVCS trend spotting and git content.

*(Credits: Branching model image was originally forked from nvie's [git-flow][15] Keynote source)*

[1]: http://git-scm.com/
[2]: http://git-scm.com/about
[3]: http://twitter.com/durdn
[4]: http://twitter.com/AtlDevTools
[5]: http://www.atlassian.com/software/stash/overview
[6]: http://www.atlassian.com
[8]: http://www.atlassian.com/software/confluence/overview/team-collaboration-software
[9]: http://www.atlassian.com/software/jira/overview
[10]: /images/articles/git-automatic-merges-with-server-side-hooks/full-branching-model.png
[11]: http://en.wikipedia.org/wiki/Fork_%28software_development%29
[12]: https://blogs.atlassian.com/2013/05/stash-git-forking-development-workflow/
[13]: https://developer.atlassian.com/display/AUI/Atlassian+User+Interface+%28AUI%29+Developer+Documentation
[14]: https://confluence.atlassian.com/display/BITBUCKET/Working+with+pull+requests
[15]: http://github.com/downloads/nvie/gitflow/Git-branching-model-src.key.zip

