---
author: Nicola Paolucci
title: "It happened: Git 2.0 is here and it's full of goodies"
date: 2014-09-03
---

This major release of `git` has been brewing for a long time and I am excited to go on the hunt in the Changelog to find cool bits of awesomeness. As usual if you want to catch up with past git releases, I've been doing this exercise for a while, check them out: [1.8.2][7], [1.8.3][16], [1.8.4][11], [1.8.5][17], [1.9][18].

This piece will necessarily cover only a selection of the release, if you want the complete list of changes and bug fixes have a look at the [full Changelog][10].

## Some defaults changing: improving usability and resolving confusion

Let's tackle the compatibility changes first. There are a few and I'd say these are welcome updates for misunderstandings that have bugged more than a beginner. They should not disrupt old experienced `git` users too much because they can very easily revert to their favorite setups by tweaking their `.gitconfig` a bit.

### **IMPORTANT!** *git push* (with no arguments) defaults to `simple` push mechanics

The default behavior of bare `git push` with no arguments has traditionally been non-intuitive: All branches were sent to the remote as long as their name matched both locally and remotely. That's what the `matching` option meant.

**In Git 2.0 the default has been changed to `simple` which is narrower in scope - more specific and more intuitive - it will now only push:**

- **The current branch to the branch with the same name only when the current branch is set to integrate with that remote branch on the same remote;**

- **The current branch to the branch with the same name, if you are pushing to a remote that is not where you usually fetch from.**

If you are nostalgic and got accustomed to the old default you can switch back setting the configuration variable `push.default` to `matching` again.

### *git add path* now equates *git add -A path*

`git add dir/` will notice paths you removed from the directory and record the removals. A great usability improvement. In older versions `git add path` used to ignore removals. You can be explicit with `git add --ignore-removal path` if you want to keep the old behavior.

`git add -u` and `git add -A` now operate on the entire tree and not on the sub-folder where you happen to run the command (this makes the two commands behave in the same way as the other `git` commands). You can still specify the `.` folder if you want the old behavior.

### *git svn* default prefixes change

The default remote prefix for `git svn` has changed in Git 2.0 and it now places remote-tracking branches under refs/remotes/origin (instead of directly under refs/remote). This can be customized using the `--prefix` option of `git svn`.

## Non-breaking changes grouped by command

The changes below do not break compatibility. They caught my eye as being useful or interesting.

### *git log* has a new option *--show-linear-break* and more

- `git log` now has a `--show-linear-break` option to display where a single strand-of-pearls is broken in its output.

- The way `git log --cc` shows a combined diff against multiple parents has been optimized.

### *git rebase* can now sign commits

- `git rebase` will interpret a lone `-` as `@{-1}`, the branch that we were previously on.

- `git rebase` (with `pull` and other commands) learned to take the `--gpg-sign` option on the command line.

### *git commit* gains a new cleanup mode and more

- `git commit --cleanup=<mode>` gained a new mode, scissors. The `--cleanup` option determines how the commit message should be cleaned up before committing. The new mode `scissors` strips leading and trailing empty lines, trailing white space, collapses consecutive empty lines and **truncates (doesn't remove) the lines commented with `#`**. More info on the various options [here][19].

- `git commit` can be told to always GPG sign the resulting commit by setting `commit.gpgsign` to `true` (which you can override on the command line with `--no-gpg-sign`).

### *git tag* now can sort numbered versions

- `git tag --list` output can be sorted using *version sort* with `--sort=version:refname`.

## Other miscellaneous changes

- `git grep` behaves now similarly to native grep when `-h` (no header) and `-c` (count) options are given.

- `git config` can now read from the standard input with `--file -` (that is a
  single dash).

- Trailing whitespaces in .gitignore files now give a warning and are ignored.

- `git pull` can now be configured to accept fast-forward only with the new "pull.ff" configuration.

- `git reset -N` will keep known paths as `intend-to-add` if the target tree-ish does not already know them.

- The bitmap-index feature from JGit has been ported, which should significantly improve performance when serving objects from a repository that uses it. See [this EclipseCon deck for more info][20].

## Conclusion

I hope you found this run down useful. Stay tuned for more on this channel! Follow me [@durdn][3] and the awesome [@AtlDevtools][4] team for more hacking and DVCS rocking.

[3]: http://twitter.com/durdn
[4]: http://twitter.com/AtlDevTools
[7]: https://blogs.atlassian.com/2013/03/highlights-new-git-1-8-2/
[10]: http://git.kernel.org/cgit/git/git.git/tree/Documentation/RelNotes/2.0.0.txt
[11]: https://blogs.atlassian.com/2013/08/what-you-need-to-know-about-the-new-git-1-8-4/
[16]: https://blogs.atlassian.com/2013/05/highlights-git-1-8-3/
[17]: https://blogs.atlassian.com/2013/12/whats-new-git-1-8-5/
[18]: https://blogs.atlassian.com/2014/02/whats-new-git-1-9/
[19]: http://git.kernel.org/cgit/git/git.git/plain/Documentation/git-commit.txt
[20]: http://www.eclipsecon.org/2013/sites/eclipsecon.org.2013/files/Scaling%20Up%20JGit%20-%20EclipseCon%202013.pdf


