---
author: Nicola Paolucci
title: "Core Concept, Workflows And Tips"
date: 2014-09-03
---

Including submodules as part of your Git development allows you to include other projects in your codebase, keeping their history separate but synchronized with yours. It's a convenient way to solve the vendor library and dependency problems. As usual with everything `git`, the approach is opinionated and encourages a bit of study before it can be used proficiently. There is already [good][2] and [detailed][1] information about `submodules` out and about so I won't rehash things. What I'll do here is share some interesting things that will help you make the most of this feature.

## Table Of Contents

1.  [Core Concept](#core)
2.  [Possible Workflows](#scenarios)
3.  [Useful Tips Incoming](#tips-intro)
4.  [How to swap a git submodule with your own fork](#tip1)
5.  [How do I remove a submodule?](#tip2)
6.  [How do I integrate a submodule back into my project?](#tip3)
7.  [How to ignore changes in submodules](#tip4)
8.  [Danger Zone! Pitfalls Interacting with Remotes](#tip5)
9.  [Conclusions](#conclusions)

## Core Concept

First, let me provide a brief explanation on a core concept about submodules that will make them easier to work with.

**Submodules are tracked by the exact _commit_ specified in the parent project, not a branch, a ref, or any other symbolic reference.**

They are _never_ automatically updated when the repository specified by the submodule is updated, only when the parent project itself is updated. As very clearly expressed in the [Pro Git chapter][2] mentioned earlier:

> When you make changes and commit in that \[submodule\] subdirectory, the
>   superproject notices that the HEAD there has changed and records the exact
>   commit you’re currently working off of; that way, when others clone this
>   project, they can re-create the environment exactly.

Or in [other words][3] :

> \[...\] git submodules \[...\] are static. Very static.  You are tracking
>   specific commits with git submodules - not branches, not references, a single
>   commit. If you add commits to a submodule, the parent project won't know. If
>   you have a bunch of forks of a module, git submodules don't care. You have
>   one remote repository, and you point to a single commit.  Until you update
>   the parent project, nothing changes.

## Possible Workflows

By remembering this core concept and reflecting on it, you can understand that `submodule` support some workflows well and less optimally others.  There are at least three scenarios where submodules are a fair choice:

*   When a component or subproject is changing too fast or upcoming changes will break the API, you can lock the code to a specific commit for your own safety.

*   When you have a component that isn't updated very often and you want to track it as a vendor dependency. I do this for my [vim plugins][4] for example.

*   When you are delegating a piece of the project to a third party and you want to integrate their work at a specific time or release. Again this works when updates are not too frequent.

[Credit][3] to _finch_ for the well-explained scenarios.

## Useful Tips Incoming

The [submodule][2] infrastructure is powerful and allows for useful separation and integration of codebases. There are however simple operations that do not have a streamlined procedure or strong command line user interface support.

If you use git submodules in your project you either have run into these or you will. When that happens you will have to look the solution up. Again and again.  Let me save you research time: [Instapaper][5], [Evernote][6] or old school [bookmark][7] this page (:D:D) and you will be set for a while.

So, here is what I have for you:

## How to swap a git submodule with your own fork

This is a very common workflow: you start using someone else's project as submodule but then after a while you find the need to customize it and tweak it yourself, so you want to fork the project and replace the submodule with your own fork. How is that done?

The submodules are stored in `.gitmodules`:

```
$ cat .gitmodules
[submodule "ext/google-maps"]
path = ext/google-maps
url = git://git.naquadah.org/google-maps.git
```

You can just edit the url with a text editor and then run the following:

```
$ git submodule sync
```

This updates `.git/config` which contains a copy of this submodule list (you could also just edit the relevant `[submodule]` section of `.git/config` manually).

[Stack Overflow reference][8]

## How do I remove a submodule?

It is a fairly common need but has a slightly convoluted procedure. To remove a submodule you need to:

1.  Delete the relevant line from the `.gitmodules` file.
2.  Delete the relevant section from `.git/config`.
3.  Run `git rm --cached path_to_submodule` (no trailing slash).
4.  Commit and delete the now untracked submodule files.

    [Stack Overflow reference][9]

## How do I integrate a submodule back into my project?

Or, in other words, how do I un-submodule a git submodule? If all you want is to put your submodule code into the main repository, you just need to remove the submodule and re-add the files into the main repo:

1.  Delete the reference to the submodule from the index, but keep the files:

    ```
    git rm --cached submodule_path (no trailing slash)
    ```

2.  Delete the `.gitmodules` file **or** if you have more than one submodules
    edit this file removing the submodule from the list:

    ```
    git rm .gitmodules
    ```

3.  Remove the `.git` metadata folder (**make sure you have backup of this**):

    ```
    rm -rf submodule_path/.git
    ```

4.  Add the `submodule` to the main repository index:

    ```
    git add submodule_path
    git commit -m "remove submodule"
    ```

**NOTE:** The procedure outlined above is _destructive_ for the history of the submodule, in cases where you want to retain a congruent history of your submodules you have to work through a fancy "merge". For more details I defer you to this very complete [Stack Overflow reference][10].

## How to ignore changes in submodules

Sometimes your `submodules` might become `dirty` by themselves. For example if you use git `submodules` to track your vim plugins, they might generate or modify local files like `helptags`. Unfortunately, [`git status`][11] will start to annoy you about those changes, even though you are not interested in them at all, and you have no intention of committing them.

The solution is very simple. Open the file `.gitmodules` at the root of your repository and for each submodule you want to ignore add `ignore = dirty`, like in this example:

```
[submodule ".vim/bundle/msanders-snipmate"]
path = .vim/bundle/msanders-snipmate
url = git://github.com/msanders/snipmate.vim.git
ignore = dirty
```

Thanks to [Nils for the great explanation][12].

## Danger Zone! Pitfalls Interacting with Remotes

As the [Git Submodule Tutorial][13] on kernel.org reminds us there are a few important things to note when interacting with your remote repositories.

The first is to **always publish the submodule change before publishing the change to the superproject that references it**. This is critical as it may hamper others from cloning the repository.

The second is to always **remember to commit all your changes before running `git submodule update`** as if there are changes they will be overwritten!

## Conclusions

Armed with these notes you should be able to tackle many common recurring workflows that come up when using submodules. In a future post I will write about alternatives to `git submodule`.

Follow me [@durdn][14] and the awesome [@AtlDevtools][15] team for more DVCS rocking.

[1]: http://speirs.org/blog/2009/5/11/understanding-git-submodules.html
[2]: http://git-scm.com/book/en/Git-Tools-Submodules
[3]: http://somethingsinistral.net/blog/git-submodules-are-probably-not-the-answer
[4]: https://bitbucket.org/durdn/cfg/src/2a79b298a32bf161d1813da9eba16e67a6a8ab1a/.vim/bundle?at=master
[5]: http://www.instapaper.com
[6]: http://www.evernote.com
[7]: http://support.mozilla.org/en-US/kb/use-bookmarks-to-save-and-organize-websites
[8]: http://stackoverflow.com/questions/11637175/swap-git-submodule-with-own-fork
[9]: http://stackoverflow.com/questions/1260748/how-do-i-remove-a-git-submodule
[10]: http://stackoverflow.com/questions/1759587/un-submodule-a-git-submodule
[11]: /tutorials/inspecting-a-repository/git-status
[12]: http://www.nils-haldenwang.de/frameworks-and-tools/git/how-to-ignore-changes-in-git-submodules
[13]: https://git.wiki.kernel.org/index.php/GitSubmoduleTutorial
[14]: http://twitter.com/durdn
[15]: http://twitter.com/AtlDevTools