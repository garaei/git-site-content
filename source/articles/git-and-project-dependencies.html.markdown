---
author: Nicola Paolucci
title: "Git and project dependencies"
date: 2014-09-03
---

Consider the following questions. How do you handle project dependencies with `git`? Our project is made up of multiple inter-dependent repositories. Currently we
 manage those with `svn:externals`. What's the best way to handle those with
 `git`? How do you split a very big repository in smaller components using git? These are some examples of the most asked questions we got at the European leg
of our recent [Getting Git Right][1] tour.

The topic appears to be a big pain point for many software teams adopting `git`,
so in this article I'll try to shed some light on the issue.

Obviously project dependencies and build infrastructure are two intertwined
areas, and even internally at Atlassian a discussion sparked on the "Future of
Builds".

Having separate repositories as opposed to having a single one can make some things harder. But it's a relatively natural – sometimes mandatory – step in the evolution of a software project for at least two major reasons: increasing build times, and shared dependencies between projects.

## Painting with broad strokes: guidelines and sub-optimal solutions

So back to the question: _How do you track and manage project dependencies with `git`?_

**If possible, you don't!**

Jokes aside, let me answer with broad strokes first and dig deeper afterwards. Please realize that **there is no silver bullet** – in `git` or otherwise – that will painlessly solve all the issues related with project dependencies.

Once a project grows over a certain size, it makes sense to split it in logical components, but don't wait to have a [100 million plus lines of code in a single repository][2] before you do. So the following are just guidelines so that you can devise your own approach.

## First choice: Use an appropriate build/dependency tool instead of git

A dependency management tool is my current recommended way forward to handle
the growing pains and the build times of sizeable projects.

Keep your modules separated in individual repositories and manage their
interdependency using a tool built for the job. There is one for (almost)
every technology stack out there. Some examples:

*   [Maven][3] (or [Gradle][4]) if you use Java
*   [Npm][5] for node apps
*   [Bower][6], [Component.io][7], etc if you use Javascript **(Updated!)**
*   [Pip][8] and requirements.txt if you use Python
*   [RubyGems][9],[Bundler][10] if you use Ruby
*   [NuGet][11] for .NET
*   [Ivy][12] (or some custom [CMake][13] action) for C++ **(Updated!)**
*   [CocoaPods][14] for Cocoa iOS apps
*   [Composer][15] or [Phing][16] for PHP **(Added!)**
*   In [Go][17] the build/dependency infrastructure is somewhat built into the language (though people have been working on a more complete solution, see [godep][18])
For our Git server [Bitbucket Server][19] we use both Maven and Bower. At build
time the tool of choice will pull the right versions of the dependencies so
that your master project can be built. Some of these tools have limitations and
make assumptions that are not optimal, but are proven and viable.

### The pains of splitting your project

Simplistically at the start of a project everything is packed in one build. But
as the project grows, that may result in your build to be too slow – at which
point you need "caching," which is where dependency management comes in. This
by the way means submodules (see below) lend themselves very well to dynamic
languages for example. Basically I think most people need to worry about build
times at some point, which is why you should use a dependency management tool.

Splitting components into separate repositories comes with some serious pain.
In no particular order:

*   Making a change to a component requires a release
*   Takes time and can fail for lots of stupid reasons
*   Feels stupid for small changes
*   It requires manually setting up new builds for each component
*   Hinders the discoverability of repositories
*   Refactoring when not all the source is available in a single repository
*   In some setups (like ours) updating APIs requires a milestone release of the
product, and then the plugin, and then the product again
We've probably missed a few things, but you get the idea. A perfect solution to
the problem is far from here.

## Second choice: Use `git submodule`

If you can't or don't want to use a dependency tool, `git` has a facility to handle `submodules`. Submodules can be convenient, especially for dynamic languages. They won't necessarily save you from slow build times, though. I already have written some [guidelines and tips][20] about them and also [explored alternatives][21]. The Internet at large also has [arguments against them][22].

### 1:1 match between `svn:external` and `git`

**BUT!** If you're looking for a 1-to-1 match between `svn:externals` and
`git,` you want to use `submodules` making sure the `submodules` track only
_release branches_ and not random commits.

## Third choice: Use other build and cross-stack dependency tools

Not always you will enjoy a project that is completely uniform and can be built
and assembled with a single tool. For example some mobile projects will need to
juggle both Java and C++ dependencies, or use proprietary tools to generate
assets. For those more complex situations you can enhance `git` with an extra
layer on top. A great example in this arena is [Android's repo][23].

Other build tools that are worth exploring:

*   [Repobuild][24]
*   [Chromium's depo_tools system][25]
*   [Distributed build][26]
*   [Facebook's Buck][27]
*   [Twitter's Pants][28]

## Conclusions and further reading

Further reads on the build infrastructure (and Maven) topic were thoughtfully
suggested by [Charles O'Farrell][29], very interesting
food for thought:

*   [In quest of the ultimate build tool][30]
*   And his follow up [Maven is broken by design][31]
*   [Recursive Maven considered harmful][32]

I'd like to conclude with this excellent quote from latter article above. Even though it is about Maven, it could equally be applied to other build and dependency tools:

> A cache does nothing except speed things up. You could remove a cache
> entirely and the surrounding system would work the same, just more
> slowly. A cache has no side effects, either. No matter what you've done
> with a cache in the past, a given query to the cache will give back the
> same value to the same query in the future.
> 
> The Maven experience is very different from what I describe! Maven
> repositories are used like caches, but without having the properties of
> caches. When you ask for something from a Maven repository, it very
> much matters what you have done in the past. It returns the most recent
> thing you put into it. It can even fail, if you ask for something
> before you put it in.

Thanks to [Charles O'Farrell][33] for the thoughtful feedback on the draft of this. I hope you enjoyed the above reflections and ping me[@durdn][34] and [@AtlDevtools][35] for more Git shenanigans.

[1]: https://www.atlassian.com/getting-git-right
[2]: https://code.facebook.com/posts/218678814984400/scaling-mercurial-at-facebook/
[3]: http://maven.apache.org/
[4]: http://www.gradle.org/
[5]: https://www.npmjs.org/
[6]: http://bower.io/
[7]: http://component.io/)
[8]: https://github.com/pypa/pip
[9]: https://rubygems.org/
[10]: http://bundler.io/
[11]: http://www.nuget.org/
[12]: https://ant.apache.org/ivy/
[13]: http://www.cmake.org/
[14]: http://cocoapods.org/
[15]: http://getcomposer.org
[16]: http://www.phing.info/
[17]: http://golang.org/
[18]: http://www.goinggo.net/2013/10/manage-dependencies-with-godep.html
[19]: http://www.atlassian.com/software/stash/overview
[20]: https://blogs.atlassian.com/2013/03/git-submodules-workflows-tips/
[21]: http://blogs.atlassian.com/2013/05/alternatives-to-git-submodule-git-subtree/
[22]: http://somethingsinistral.net/blog/git-submodules-are-probably-not-the-answer/
[23]: http://source.android.com/source/developing.html
[24]: https://github.com/chrisvana/repobuild/wiki/Motivation
[25]: http://dev.chromium.org/developers/how-tos/depottools
[26]: http://typesafehub.github.io/distributed-build/
[27]: http://facebook.github.io/buck/
[28]: http://pantsbuild.github.io/
[29]: http://blog.charleso.org/
[30]: http://blog.ltgt.net/in-quest-of-the-ultimate-build-tool/
[31]: http://blog.ltgt.net/maven-is-broken-by-design
[32]: http://blog.lexspoon.org/2012/12/recursive-maven-considered-harmful.html
[33]: https://twitter.com/charlesofarrell
[34]: http://twitter.com/durdn
[35]: http://twitter.com/AtlDevTools
[36]: https://www.atlassian.com/solutions/git-essentials/?utm_source=Atlassian&amp;utm_medium=blog&amp;utm_campaign=git-together